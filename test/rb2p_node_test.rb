
require 'rb2p/node'
require "test_helper"

require 'json'

class Rb2pNodeTest < Minitest::Test
    def test_node
        n1 = Rb2p::Node.new 'port' => 9090
        n2 = Rb2p::Node.new 'port' => 9091

        n1.add_neighbor 'host' => 'localhost', 'port' => 9091

        n1.run_once
        n2.run_once

        n1.run_once
        n2.run_once

        n1.shutdown
        n2.shutdown
    end
end