require 'rb2p/messages'

require "test_helper"

require 'json'

class Rb2pMessagesTest < Minitest::Test

  def test_construction
    msg = Rb2p::Messages::Message.new("msgid", "to", "from", 3, "text/plain", 3, "Hi.")
    assert msg.to == "to"
    assert msg.from == "from"
    assert msg.length == 3
    assert msg.type == "text/plain"
    assert msg.body == "Hi."

    JSON::pretty_generate(msg.to_h)

    msg = Rb2p::Messages::CallSetup.new("id", "to", "from", 3)

    msg = Rb2p::Messages::CallTeardown.new("id", "to", "from")

    msg = Rb2p::Messages::CallEstablished.new("id", "to", "from")

    msg = Rb2p::Messages::CallMessage.new("id", "to", "from", "text/plain", 3, "Hi.")
    assert msg.to == "to"
    assert msg.from == "from"
    assert msg.length == 3
    assert msg.type == "text/plain"
    assert msg.body == "Hi."

  end

  def test_messages_to_from_json
    msg = Rb2p::Messages::Message.new("msgid", "to", "from", 3, "text/plain", 3, "Hi.")
    assert msg.to == "to"
    assert msg.from == "from"
    assert msg.length == 3
    assert msg.type == "text/plain"
    assert msg.body == "Hi."

    jmsg = JSON::pretty_generate(msg.to_h)

    msg2 = JSON::parse(jmsg)

    msg3 = Rb2p::Messages::Message.new
    msg3.members.each do |sym|
      msg3[sym] = msg2[sym.to_s]
    end

    assert_equal msg, msg3
  end

  def test_messages_to_from_json2
    msg = Rb2p::Messages::Message.new("msgid", "to", "from", 3, "text/plain", 3, "Hi.")

    assert msg.to == "to"
    assert msg.from == "from"
    assert msg.length == 3
    assert msg.type == "text/plain"
    assert msg.body == "Hi."
    
    msg2 = Rb2p::Messages::to_message(Rb2p::Messages::to_json(msg))

    assert_equal msg, msg2
  end

end
