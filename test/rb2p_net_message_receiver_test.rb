require 'rb2p/net/message_receiver'
require "test_helper"

require 'json'

class Rb2pNetMessagesReceiverTest < Minitest::Test
    def test_sending_messages
        m = Rb2p::Net::MessageReceiver.new
        m.add_server(0)
        port = m.servers[0].socket.addr[1]
        m.add_connection('127.0.0.1', port)
        m.select
        
        arr = []

        buf1 = m.buffers[-2]
        buf2 = m.buffers[-1]
        buf2.msg_handlers << proc do |type, msg, b|
            arr = [type, msg, b]
        end

        v = buf1.send("msgtype", "msgdata")
        assert_equal v, 22

        # Let async stuff sync.
        sleep 0.1

        m.select(3)

        assert_equal arr[0], "msgtype"
        assert_equal arr[1], "msgdata"
        refute_nil arr[2]

        m.shutdown
    end
end