require 'json'


module Rb2p
    module Messages

        def self.to_json(msg)
            h = msg.to_h
            h['msgtype'] = case msg
            when CallSetup
                'rb2p/callsetup'
            when CallTeardown
                'rb2p/callteardown'
            when CallEstablished
                'rb2p/callestablished'
            when CallMessage
                'rb2p/callmessage'
            when WhoAmIMessage
                'rb2p/whoami'
            when Message
                'rb2p/message'
            when NeighborsMessage
                'rb2p/neighbors'
            else
                throw Exception.new("Unknown message type: "+msg.class.to_s)
            end

            JSON::pretty_generate(h)
        end

        def self.to_message(h)
            h = JSON::parse(h)

            msg = case h['msgtype']
            when 'rb2p/message'
                Message.new
            when 'rb2p/callsetup'
                CallSetup.new
            when 'rb2p/callteardown'
                CallTeardown.new
            when 'rb2p/callestablished'
                CallEstablished.new
            when 'rb2p/callmessage'
                CallMessage.new
            when 'rb2p/whoami'
                WhoAmIMessage.new
            when 'rb2p/neighbors'
                NeighborsMessage.new
            else
                throw Exception.new("Unknown message type: "+msg)
            end

            msg.members.each do |sym|
                msg[sym] = h[sym.to_s]
            end

            msg
        end

        Message = Struct.new(
            :to,
            :from,
            :visited,            
            :ttl,
            :type,
            :body
        )

        WhoAmIMessage = Struct.new(
            :node_id
        )

        NeighborsMessage = Struct.new(
            :node_id,
            :neighbors
        )

        # A message with an empty body that sets up a path across nodes for more efficent transfers.
        # The +type+ is set to "rb2p/callsetup".
        # The +to+ is the destination node ID to set the call up with.
        # The +from+ is set to the the call id that the sending node should use in the +to+ field.
        CallSetup = Struct.new(
            :id,
            :to,
            :from,
            :ttl
        )

        # A message that destroys the call.
        CallTeardown = Struct.new(
            :id,
            :to,
            :from
        )


        # When a call is setup, this message informs the initiator that it is ready.
        # The +from+ field is what outbound messages must have their +from+ value set to.
        # The +to+ field is set to what the +from+ field was when the message was sent.
        CallEstablished = Struct.new(
            :id,
            :to,
            :from
        )

        # A message that must be routed through a call. 
        # The +to+ and +from+ fields are the call ID.
        CallMessage = Struct.new(
            :id,
            :to,
            :from,
            :type,
            :length,
            :body
        )
    end
end
