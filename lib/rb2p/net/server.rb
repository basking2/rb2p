module Rb2p
    module Net
        class Server
            attr_reader :socket

            attr_accessor :err_handlers

            # Similar to +msg_handler+ for a Buffer, this is a list of callbacks to handle new connections.
            # A callback takes two arguments, the socket and this Server object.
            attr_accessor :con_handlers

            def initialize(*args)
                @socket = TCPServer.new(*args)
                @socket.listen(10)
                @err_handlers = []
                @con_handlers = []
            end

            # Returns sock.addr which is a list of  [address_family, port, hostname, numeric_address].
            def connect
                socket = @socket.accept
                @con_handlers.each do |h|
                    h.call(socket, self)
                end
            end

            def shutdown
                @socket.close
            end
        end # class Server
    end # module Net
end # module Rb2p