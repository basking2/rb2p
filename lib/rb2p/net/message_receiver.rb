require 'socket'

require 'rb2p/net/buffer'
require 'rb2p/net/server'

module Rb2p
    module Net

        # A class that reads data from Server or Buffer objects.
        # Both classes have a +.socket+ attribute that is passed to +select()+.
        # When data is ready on the socket, the owning class is called.
        #
        # In the case of the Server class, the +connect+ method is called.
        #
        # In the case of the Buffer class, the +receive+ or +send+ method is called.
        #
        # On an error the object's +.err_handlers+ attribute is traversed and each error hander is
        # +call+ end, receiving the object (Server or Buffer) that suffered the error.
        # 
        # Notice that this class has error handlers that are passed to all newly created Buffer or Server
        # objects. In that way, you may have individual or common error handlers, or any mix of the two.
        #
        class MessageReceiver

            # Buffers
            attr_accessor :buffers

            # How we get connected to and make buffers.
            # This is typically only one server.
            attr_accessor :servers

            # Error handlers new network brokers get.
            attr_accessor :err_handlers

            # Message handlers new buffers get.
            attr_accessor :msg_handlers

            # When a new buffer is added, these handlers are called with the new buffer as the only argument.
            attr_accessor :buf_handlers

            def initialize
                @buffers = []
                @servers = []
                @err_handlers = []
                @msg_handlers = []
                @buf_handlers = []
            end

            # Returns the created Buffer object.
            def add_socket(sock)
                b = Buffer.new(sock, @msg_handlers.dup, @err_handlers.dup)
                
                @buf_handlers.each do |fn|
                    fn.call(b)
                end

                @buffers << b
                b
            end

            # Returns the created Buffer object.
            def add_connection(host, port)
                begin
                    s = TCPSocket.new(host, port)                
                    b = add_socket(s)
                    b
                rescue Errno::ECONNREFUSED
                    nil
                end
            end

            # Returns the creatd Server object.
            def add_server(*args)
                 ms = Server.new(*args)
                 ms.err_handlers = err_handlers.dup
                 ms.con_handlers << proc do |sock, svr|
                    self.add_socket(sock)
                 end
                 @servers << ms
                 ms
            end

            def delete_buffer(buf, call_error_handlers = false)
                case buf
                when Server
                    @servers.delete(buf)
                when Buffer
                    @buffers.delete(buf)
                end

                if call_error_handlers
                    buf.err_handlers.each { |eh| eh.call(buf) }
                end
            end


            # Select waits for any data and tries to build a message.
            #
            # If a message is recieved it is passed to the given block or returned as an enumeration.
            #
            # This will also create new receive buffers if a server socket has a waiting 
            # connection on it. In that case, this may return sooner than the timeout
            # with no data being processed.
            #
            # This may also return sooner than the timeout if some data was received, but
            # not enough to make a final message.
            #
            # +timeout+:: The seconds to wait for any data.
            #
            def select(timeout = 3)
                socket_map = @buffers.map {|b| [ b.socket, b ] }.to_h

                # Merge in the server sockets.
                socket_map.merge!(@servers.map { |s| [s.socket, s]}.to_h)

                # The buffer sockets for whose buffer objects have data to write.
                # We do not need this to be a hash because the mapping from socket->object 
                # is provided by socket_map.
                socket_write_set = @buffers.select { |buf| buf.send_buffer && buf.send_buffer.length > 0 }.to_h.keys

                r, w, e = IO.select(socket_map.keys, socket_write_set, socket_map.keys, timeout)

                # Handle error sockets.
                (e || []).
                    each do |sock|
                        obj = socket_map[sock]
                        delete_buffer(obj, true)
                    end

                (w || []).
                    each do |sock|
                        obj = socket_map[sock]

                        case obj
                        when Buffer
                            begin
                                obj.send
                            rescue EOFError
                                Rb2p::logger.debug("Buffer EOF.")                                
                                delete_buffer(obj, true)
                            rescue
                                delete_buffer(obj, true)
                            end
                        else
                            raise Exception.new("Unhandled type: #{obj}.")
                        end
                    end

                (r || []).
                    map do |sock|

                        obj = socket_map[sock]

                        case obj
                        when Server
                            obj.connect
                        when Buffer
                            begin
                                obj.recv
                            rescue EOFError
                                Rb2p::logger.debug("Buffer EOF.")                                
                                delete_buffer(obj, true)
                            rescue => e
                                Rb2p::logger.debug(e)
                                delete_buffer(obj, true)
                            end
                        else
                            raise Exception.new("Unhandled type: #{obj}.")
                        end
                    end.
                    select do |msg|
                        msg
                    end
            end

            def shutdown
                @servers.each do |svr|
                    svr.shutdown
                end

                @buffers.each do |buf|
                    buf.shutdown
                end
            end
        end
    end
end
