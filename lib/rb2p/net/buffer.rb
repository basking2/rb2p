module Rb2p
    module Net
        # The protocol version, 0x1.
        VERSION = 0x1

        # The length of a message header, which is 8 bytes.
        HEADER_LENGTH = 8

        # send:: Send some bytes from the send_buffer.
        # recv:: Receive some bytes from the socket. If we have enough for a message, call our callbacks.
        #
        class Buffer

            attr_reader :socket

            #  A list of callabls to handle getting messages.
            attr_accessor :msg_handlers

            # A list of callables to handle errors.
            attr_accessor :err_handlers

            # How much data is to be sent by this.
            attr_accessor :send_buffer

            def initialize(socket, msg_handlers, err_handlers)
                @socket = socket
                @err_handlers = err_handlers
                @msg_handlers = msg_handlers
                reset_header
            end

            # Receiver from the socket.
            # If a full message is received, it is returned. Otherwise nil is returned.
            def recv
                recv_header
                recv_type
                msg = recv_data

                if msg
                    @msg_handlers.each do |handler|
                        handler.call(@type, @data, self)
                    end

                    reset_header                     
                end
            end

            # Buffer the whole message to be sent and then tries to send as much of it as it can.
            #
            # Returns the number of bytes actually sent.
            #
            # +type+:: A string.
            # +message+:: The message body. A string.
            def send(type = nil, message = nil)
                # If data is given, schedule it to be sent.
                if type && message
                    header = [VERSION, type.length, message.length].pack('nnN')
                    @send_buffer += header
                    @send_buffer += type
                    @send_buffer += message
                end

                begin
                    # Always try to send something.
                    sent = @socket.sendmsg_nonblock(@send_buffer)
                    @send_buffer = @send_buffer[sent..-1]
                    Rb2p.logger.debug { "SENT #{sent} of  #{message}" } if Rb2p::debug
                    sent
                rescue IO::WaitWritable
                    # Pass.
                    0
                end
            end

            def shutdown
                @socket.close
            end

            private

            def read_data(len)
                begin
                    @data += @socket.read_nonblock(len)
                rescue IO::WaitReadable
                    # Pass.
                end
            end

            # Try to parse the header.
            def recv_header
                if ! @header_read

                    # Read whatever's left.
                    read_data(HEADER_LENGTH - @data.length)
                    
                    if @data.length >= HEADER_LENGTH
                        # 2x16 bytes and 1x4 byte.
                        version, @type_length, @length = @data.unpack('nnN')

                        if version != VERSION
                            raise Exception.new("Unsupported version: %d"%version)
                        end

                        @data = ''
                        @header_read = true
                    end
                end
            end

            def recv_type
                if @header_read && ! @type_read
                    read_data(@type_length - @data.length)
                    if @data.length >= @type_length
                        @type = @data
                        @data = ''
                        @type_read = true
                    end
                end
            end

            def recv_data
                if @header_read && @type_read
                    read_data(@length - @data.length)
                    if @data.length >= @length
                        @data
                    else
                        nil
                    end
                end
            end

            # Reset the header data.
            def reset_header
                @header_read = false
                @type_read = false
                @type = nil
                @length = nil
                @type_length = nil
                @data = ''
                @send_buffer = ''
            end
        end
    end
end
