require 'rb2p/node'

require 'thread'

module Rb2p
    # A class that wraps an Rb2p::Node to provide asynchronous interactions.
    # 
    # The intent is that this is a buffering class between a user interface and the 
    # Rb2p::Node.
    class AsyncNode

        attr_reader :node

        # Queue the user may write to.
        attr_reader :input_queue

        # Queue the user may read from.
        # Care must be taken to drain this or data could build up and eventually be thrown away.
        attr_reader :output_queue

        # +settings+:: Hash of settings passed to Rb2p::Node.new. 
        #              Settings may be an instance of Rb2p::Node in which case it is directly used.
        #              This is useful when callbacks should be set up before the node's Rb2p::Node#run 
        #              method is called.
        # +node_callback+:: If given, this is user code that is executed in the node's run thread
        #                   after the node executes.
        def initialize(settings, &node_callback)
            @input_queue = Queue.new
            @output_queue = Queue.new
            @node = if settings.is_a? Rb2p::Node
                settings
            else
                Rb2p::Node.new(settings)
            end

            # Build the callback that will run in the node thread.
            node_callback_internal = proc do 
                node_callback.call if node_callback

                while @input_queue.length > 0
                    msg = @input_queue.deq
                    if msg.is_a?(Array) && msg.length == 2
                        @node.send_message(*msg)
                    elsif msg.is_a?(Array) && msg.length == 3
                        @node.send_message(*msg)
                    else
                        Rb2p::logger.error("Unhandled message: #{msg}.")
                    end
                end
            end

            # Build the node thread.
            @node_thread = Thread.new do
                @node.run &node_callback_internal
            end
        end

        # Enqueue a 3-element object to represent sending a message. This is +[type, body, to]+.
        def send_message(type, body, to="*")
            @input_queue.enq([type, body, to])
        end

        # Close the input queue and shutdown the Rb2p::Node instance. The Rb2p::AsyncNode#node_thread
        # thread will eventually exit. You can use #wait if you must block for that event.
        def shutdown
            @input_queue.close
            @node.shutdown
        end

        # Block for the Rb2p::Node management thread to join.
        def wait
            @node_thread.join
        end
    end

end