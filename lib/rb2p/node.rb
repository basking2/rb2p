require 'rb2p/net/message_receiver'

require 'rb2p/neighbor'
require 'rb2p/messages'

module Rb2p

    # A Rb2p node.
    class Node
        attr_reader :node_id

        attr_reader :settings

        # A mapping of node ids to neighbor records.
        attr_reader :neighbors

        attr_reader :message_receiver

        # A hash that maps message type to a handler function that takes the +type+, +msg+, and the recieving +buffer+.
        attr_reader :message_handlers

        # A proc that takes the type, msg, and buffer for any unknown message.
        attr_accessor :default_message_handler

        # A proc that takes the type, msg, and buffer for +rb2p/message+ messages that this node should 
        # consume.
        #
        # [NOTE]
        # Unlike other handlers, the +msg+ argument is a parsed Rb2p::Messages::Message.
        attr_accessor :consume_message_handler

        # A proce that takes the type, msg, and buffer for +rb2p/message+ messages, decrements the +ttl+ field and
        # relays the message other nodes.
        #
        # [NOTE]
        # Unlike other handlers, the +msg+ argument is a parsed Rb2p::Messages::Message.
        #
        # You probably do not want to replace this, but perhaps you may want to intercept and augment this.
        attr_accessor :relay_message_handler

        # A default port, 9090.
        PORT = 9090

        # +settings+ may the options...
        # +id+:: Any short random string to uniquely identify the node.
        # +port+:: The port to open a server on to listen. The default is Rb2p::Node::PORT.
        # 
        def initialize(settings = {})
            @node_id = (settings['id'] || rand(0..65536)).to_s
            @settings = settings
            @neighbors = {}
            @wanted_buffers = settings[:wanted_buffers] || 5
            @message_receiver = Rb2p::Net::MessageReceiver.new
            @message_handlers = {}
            @default_message_handler = proc do |type, msg, buf|
                m = Rb2p::Messages::to_message(msg)
                Rb2p::logger.info "DEFAULT HANDLER - Got message of type #{type}\n #{msg}"
            end

            @port = settings['port'] || PORT

            # Use a list so we can sort the order we want to visit them in.
            @unconnected_neighbors = []

            # Respond to all error by logging them.
            @message_receiver.err_handlers << proc do |buf|
                case buf
                when Rb2p::Net::Server
                    Rb2p::logger.info("Server died.")
                when Rb2p::Net::Buffer
                    Rb2p::logger.info("Buffer died.")
                end
            end

            @relay_message_handler = proc do |type, msg, buf|
                msg['ttl'] -= 1
                msg['visited'] = (msg['visited'] || []).push(@node_id)
                Rb2p::logger.debug("Relaying message #{msg}.")
                m = Rb2p::Messages::to_json(msg)

                @message_receiver.buffers.each do |b|
                    # Don't send to the node that sent this to us.
                    next if b === buf

                    # Relay.
                    Rb2p::logger.debug("Relaying message to #{b}.")
                    b.send(type, m)
                end
            end

            @consume_message_handler = proc do |type, msg, buf|
                Rb2p::logger.info "DEFAULT CONSUME MESSAGE HANDLER - Got message of type #{type}\n #{msg}"
            end

            # Respond to all messages by logging them.
            @message_receiver.msg_handlers << proc do |type, msg, buf|
                case type
                when 'rb2p/message'
                    relay_message(type, msg, buf)
                when 'rb2p/whoami'
                    # NOP
                    #
                    # The Rb2p::Net::Buffer callbacks will update the node record ID.
                    # Rb2p::logger.debug("Got whoami message in message_receiver handler. Taking no action in this handler.")
                when 'rb2p/neighbors'
                    m = Rb2p::Messages::to_message(msg)
                    m.neighbors.each do |key, val|
                        set_neighbor(val)
                    end
                else
                    if @message_handlers.has_key? type
                        @message_handlers[type].call(type, msg, buf)
                    else
                        @default_message_handler.call(type, msg, buf)
                    end
                end
            end

            # Respond to new buffers by sending our WhoAmI.
            @message_receiver.buf_handlers << proc do |buf|
                # Tell the new buffer who we are.
                m = Rb2p::Messages::WhoAmIMessage.new(@node_id)
                m = Rb2p::Messages::to_json(m)
                buf.send('rb2p/whoami', m)
            end

            self.add_server(@port)
        end

        # Relay a message that has an id, to, from, ttl fields and a visited list of ids.
        #
        # +type+:: The type. A string.
        # +msg+:: A JSON parsable message with required fields.
        # +buf+:: The buffer that received this message. We do not forward to that buffer.
        #
        def relay_message(type, msg, buf)

            msg = Rb2p::Messages::to_message(msg)

            if (msg['visited'] || []).member?(@node_id)
                # NOP - The message is not for us and we've listed ourselves as having relayed it already.
            elsif msg['to'] == @node_id
                @consume_message_handler.call(type, msg, buf)
            elsif msg['from'] == @node_id
                # NOP - This is our message.
            elsif msg['to'] == "*"
                @consume_message_handler.call(type, msg, buf)
                @relay_message_handler.call(type, msg, buf) if msg['ttl'] > 0
            elsif msg['ttl'] == 0
                # NOP - The message is not for us and is expired.
            else
                @relay_message_handler.call(type, msg, buf)
            end
        end

        def add_server(port)
            s = @message_receiver.add_server(port)
            if s
                s.err_handlers << proc do |svr|
                    self.add_server(port)
                end
            end
        end

        # Broadcast a message to the overlay network. The :to field is the intended recipient or * if all nodes should receive this.
        def send_message(type, body, to = "*")
            m = Rb2p::Messages::Message.new(to, @node_id, [ @node_id ], 5, type, body)

            m = Rb2p::Messages::to_json(m)

            @message_receiver.buffers.each do |b|
                b.send('rb2p/message', m)
            end
        end

        # Create a Rb2p::Neighbor record from a hash of information.
        # Put the built neighbor in the +unconnnected_neighbors+ list so that when more node connections
        # are needed, it will be used.
        def set_neighbor(opts)

            return unless opts

            node_id = (opts.member? 'node_id')? opts['node_id'].to_s : nil

            # Don't set ourselves in our own node database.
            return if node_id == @node_id
            
            # Not having a host value is an error.
            host = opts['host'] || (raise Exception.new("Host required for a neighbor."))

            # Not having a port, we may use a default.
            port = opts['port'] || 9090

            # The ID will be the combination of port and host.
            id = "#{host}:#{port}"

            n = if @neighbors.member? id
                    @neighbors[id]
                else
                    n = Rb2p::Neighbor.new
                    @neighbors[id] = n
                    @unconnected_neighbors.push(n)
                    n
                end

            n.host = host
            n.port = port
            n.id = id
            n.node_id = node_id
            n.lastseen = Time.at(0)
            n.sent = 0
            n.received = 0
        end

        def share_neighbors
            m = Rb2p::Messages::NeighborsMessage.new(
                @node_id,
                @neighbors.map do |key, val|
                    [key, { 'host'=>val['host'], 'port'=> val['port'], 'node_id' => val['node_id']}]
                end.to_h
                )
            m = Rb2p::Messages::to_json(m)
            broadcast('rb2p/neighbors', m)
        end

        def broadcast(type, message)
            @message_receiver.buffers.each do |b|
                b.send(type, message)
            end
        end

        # Run until @running = false.
        #
        # This will block for some time, waiting for network events.
        # Then, if a block is given, that block will be called.
        # Then this blocks again for more network events.
        # 
        # The intention of the block being given is to allow the user to perform 
        # periodic maintenance on the node between blocking on sockets for IO.
        def run
            @running = true
            Rb2p.logger.debug "Running."
            while @running
                run_once

                block_given? && yield
            end
        end

        def run_once
            # Try to build all the connections we want. Do this before IO work.
            connect_buffers

            # Randomly share our neighbors.
            share_neighbors if rand(1000) == 0

            @message_receiver.select(1)

        end

        # If we have fewer connections than we like, connect more nodes.
        #
        # This is best-effort aglorithm and may not succeed in fulfilling the @wanted_buffers count.
        #
        def connect_buffers
            i = @wanted_buffers - @message_receiver.buffers.length
            while i > 0
                b = connect_one_buffer
                break if b.nil?
                i -= 1
            end
        end

        # Do the work of selecting a single neighbor and connecting to it as a Buffer object.
        # This returns the buffer that was connected or nil if none was.
        def connect_one_buffer
            # Get a neighbor to try.
            n = select_neighbor

            # If we can't find one we may try, or if the list is empty, abort our attempts.
            return nil unless n

            b = @message_receiver.add_connection(n.host, n.port)

            if b
                # When a buffer experiences an error, it is disconnected and put on the unconnected_neighbors list.
                b.err_handlers << proc do |sock, buffer|
                    # On error / disconnect, put us back in the unconnected list.
                    @unconnected_neighbors.push(n)
                end

                # When a buffer gets a WhoAmI from a node, update its neighbor record.
                b.msg_handlers << proc do |type, msg, buff|
                    if type == 'rb2p/whoami'
                        m = Rb2p::Messages::to_message(msg)
                        Rb2p.logger.debug("Updating last seen for #{n.id} with node id of #{m.node_id}.")
                        n.lastseen = Time.now
                        n.node_id = m.node_id.to_s
                    end
                end
            else
                # On a failed connection, put us back in the disconnected list.
                @unconnected_neighbors.push(n)
            end

            b
        end

        # This removes a neighbor from the @unconnected_neighbors list and returns it.
        # This may also return nil if no suitable neighbor is found in the list.
        def select_neighbor
            tries = @unconnected_neighbors.length
            n = nil
            tries.times do 
                n = @unconnected_neighbors.shift

                # Drop and ignore nodes that share our node_id.
                # Perhaps one of our neighbors told us we were connected to ourselves?
                if n.node_id == @node_id
                    n = nil
                    next
                end

                # If we have seen a neighbor recently, don't try to connect to it very quickly.
                if Time.now - n.lastseen < 600
                    @unconnected_neighbors.push n
                    n = nil
                    next
                end

                # If we get to the end of the loop without calling next, break out. We've found a suitable n.
                break
            end

            n
        end

        def shutdown
            @running = false
        end
    end
end