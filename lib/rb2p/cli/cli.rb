module Rb2p
    module Cli
        def self.run(async_node, stdin=STDIN, stdout=STDOUT, stderr=STDERR)

            # Register a way to display incoming messages.
            async_node.node.consume_message_handler = proc do |type, msg, buf|
                case msg.type
                when 'msg'
                    puts "\tMessage from #{msg.from} to #{msg.to}:\n\t#{msg.body.gsub("\n", "\n\t")}\n"
                when 'ping'
                    async_node.send_message('pong', 'Ping reply.', msg.from)
                when 'pong'
                    puts "Ping reply from #{msg.from} with ttl #{msg.ttl}."
                else
                    Rb2p::logger.info { "Received a user message: #{msg}" }
                end
            end

            async_node.send_message('ping', 'Ping!')


            loop do
                l = begin
                    print "> "
                    (STDIN.gets("\n") || 'exit').strip
                rescue IOError
                    nil
                end
        
                if l.nil? || l =~ /q|quit|exit/
                    stdout.write("Shutting down and exiting.")
                    async_node.shutdown
                    break
                elsif not l.empty?
                    handle_command(async_node, l)
                else
                    stdout.write("Unhandled command line: #{l}\n")
                end
            end
        end # self.run

        def self.handle_command(async_node, cmd)
            case cmd
            when /^nei(?:gh(?:bors)?)?/
                # Print neighbors.
                puts "id, host, port, sent, received, last seen"
                async_node.node.neighbors.each do |key, val|
                    puts "#{val.node_id} #{val.host} #{val.port} #{val.sent} #{val.received} #{val.lastseen}"
                end
            when /^id/
                # Print our node id.
                puts "Node ID: #{async_node.node.node_id}"
            when /^share neighbors/
                async_node.node.share_neighbors
            when /^buffers/
                async_node.node.message_receiver.buffers.each do |b|
                    puts b
                end
            when /^ping\s*(.+)?$/
                # Ping everyone, or optionall a single node.
                to = $~[1]
                if to
                    async_node.send_message('ping', 'Ping!', to)
                else
                    async_node.send_message('ping', 'Ping!')
                end
            else
                # Msg.
                async_node.send_message("msg", cmd)
            end
        end
    end # Cli
end # Rb2p