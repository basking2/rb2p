
module Rb2p

    # A record representing information collected about another node in our network.
    Neighbor = Struct.new(
        :id,
        :node_id,
        :host,
        :port,
        :lastseen,
        :sent,
        :received
    ) do
    end

end