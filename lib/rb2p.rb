require 'rb2p/version'
require 'logger'

module Rb2p
  class Error < StandardError; end

  @@logger = Logger.new(STDOUT, :progname => "Rb2p")

  @@debug = false

  def self.logger
    @@logger
  end

  def self.logger=(logger)
    @@logger = logger
  end

  def self.debug
    @@debug
  end

  def self.debug=(debug)
    @@debug = debug
  end

end
