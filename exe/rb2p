#!/usr/bin/env ruby

require "rb2p"
require 'rb2p/node'
require 'rb2p/async_node'
require 'rb2p/cli'
require 'optparse'
require 'yaml'
require 'pp'

CONFIG = {
    'port' => 9090,
    'id' => (rand * 100000).to_i,
    'seed_nodes' => []
}

default_config = File.join(ENV['HOME'], '.rb2p.yaml')
if File.file? default_config
    c = YAML::load(File.read(default_config))
    CONFIG.merge!(c)
end

OptionParser.new do |opts|
    opts.on('-p', '--port=port', Integer, "The port to listen on.") do |p|
        CONFIG['port'] = p
    end

    opts.on('-n', '--node=host[:port]', String, "Add a node to connect to.") do |n|
        i = n.rindex(':')
        if i
            host = n[0..i-1]
            port = n[i+1..-1]
        else
            host = n
            port = 9090
        end
        CONFIG['seed_nodes'] << { 'host' => host, 'port' => port }
    end

    opts.on('--debug') do
        Rb2p::debug = true
    end

    opts.on('-v', '--verbose', "Verbose.") do 
        CONFIG['verbose'] = true
    end
end.parse! ARGV

if CONFIG['verbose']
    Rb2p.logger.level = 'DEBUG'
    Rb2p.logger.debug "Configuration"
    Rb2p.logger.debug YAML::dump(CONFIG)
end

node = Rb2p::Node.new('port' => CONFIG['port'], 'id' => CONFIG['id'])

CONFIG['seed_nodes'].each do |n|
    node.set_neighbor(n)
end

async_node = Rb2p::AsyncNode.new(node) do
    # Nop.
end

# Handle ^C. We close STDIN to force the CLI client to quit.
Signal.trap('INT') do
    STDIN.close
    async_node.shutdown
end

Rb2p::Cli::run(async_node)

async_node.wait
